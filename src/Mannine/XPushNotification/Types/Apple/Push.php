<?php namespace Mannine\XPushNotification\Types\Apple;

use Apple\ApnPush\Feedback;

use Apple\ApnPush\Notification;
use Apple\ApnPush\Notification\Message;
use Apple\ApnPush\Notification\Connection;
use Apple\ApnPush\Notification\SendException;

class Push {

    /**
     * List of Push hosts
     * @var array
     */
    private $host   = array(
        'dev'           => 'ssl://gateway.sandbox.push.apple.com:2195',
        'sandbox'       => 'ssl://gateway.sandbox.push.apple.com:2195',
        'production'    => 'ssl://gateway.push.apple.com:2195',
    );

    /**
     * Config
     * @var array
     */
    private $config = array();

    /**
     * Certificate file
     * @var
     */
    private $certificate;

    /**
     * @var
     */
    private $passphrase;

    /**
     * @var array
     */
    protected $addresses = array();

    /**
     * @var array
     */
    protected $payload = array();


    protected $log      = array();

    /**
     * @param $config
     * @param $addresses
     * @param $payload
     */
    public function __construct($config, $addresses, $payload){

        //set config
        $this->config           = $config["apple"];

        //set addresses
        $this->addresses        = $addresses;

        //set payload
        $this->payload["aps"]   = $payload;

        if(file_exists($this->config["certificates"][$this->config["environment"]]))
            $this->certificate = $this->config["certificates"][$this->config["environment"]];
        else
            throw new \Exception("Certificate was not found: ".$this->config["certificates"][$this->config["environment"]]);

        $this->passphrase = $this->config["passPhrase"];
    }

    /**
     * Send message to apple services
     * @return array
     */
    public function send(){
        
        //create connection object
        $connection = new Connection($this->certificate, $this->passphrase, $this->config["environment"] && $this->config["environment"] == 'production' ? false : true);

        //create notification object
        $notification   = new Notification($connection);

        //send messages to addresses
        foreach($this->addresses as $key => $value){

            try {

            //create message object
            $message = new Message;

            //set push token
            $message->setDeviceToken($value);

            //set realtime wakeup applicatopn
            $message->setContentAvailable(1);

            //set custom data
            if(isset($this->payload["aps"]["tds"])){
                $message->setExtra(array(
                    "tds" => $this->payload["aps"]["tds"]
                ));    
            }

            //set push body text
            if(isset($this->payload["aps"]["alert"])){
                $message->setBody($this->payload["aps"]["alert"]);    
            }
            
            //set sound
            if(isset($this->payload["aps"]["sound"])){
                $message->setSound($this->payload["aps"]["sound"]);
            }

            //send push message
            $notification->send($message);

            $this->log["success"][] = $value;

            } catch (SendException $error) {
                $this->log["error"][] = array(
                    "token"=>$value,
                    "error" => (string) $error
                );
            }
        }

        return $this;
    }

    /**
     *
     */
    public function getLog(){
        return $this->log;
    }

}